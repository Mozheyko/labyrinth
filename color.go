package labyrinth

import (
	"image/color"
)

const ColorMode_BGWhite_LineBlack = 1
const ColorMode_BGBlack_LineWhite = 0

var (
	ColorBlack       = color.RGBA{0, 0, 0, 255}
	ColorWhite       = color.RGBA{255, 255, 255, 255}
	ColorRed         = color.RGBA{255, 0, 0, 255}
	ColorGreen       = color.RGBA{0, 255, 0, 255}
	ColorBGDefault   = ColorBlack
	ColorLineDefault = ColorWhite
)

func getBGColor(mode int) color.RGBA {
	if mode == ColorMode_BGBlack_LineWhite {
		return ColorBlack
	}
	if mode == ColorMode_BGWhite_LineBlack {
		return ColorWhite
	}
	return ColorBGDefault
}

func getLineColor(mode int) color.RGBA {
	if mode == ColorMode_BGBlack_LineWhite {
		return ColorWhite
	}
	if mode == ColorMode_BGWhite_LineBlack {
		return ColorBlack
	}
	return ColorLineDefault
}
