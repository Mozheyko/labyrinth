package labyrinth

import (
	"log"
	"math/rand"
	"os"
	"strings"
	"testing"
	"time"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func TestGenerate(t *testing.T) {
	fileName := "test"
	setting := NewSettingTest()
	setting.File = fileName
	mainFileName, trackFileName, err := Generate(setting)
	if err != nil {
		t.Errorf("Error Generate: %v", err)
	}
	if strings.Index(mainFileName, fileName) == -1 {
		t.Errorf("Error Generate mainFileName not found")
	}
	if strings.Index(trackFileName, fileName) == -1 {
		t.Errorf("Error Generate mainFileName not found")
	}

	err = os.Remove(mainFileName)
	if err != nil {
		log.Println(err)
	}
	err = os.Remove(trackFileName)
	if err != nil {
		log.Println(err)
	}
}

func BenchmarkGenerate(b *testing.B) {
	fileName := "test"
	mainFileName := ""
	trackFileName := ""
	setting := NewSettingTest()
	setting.File = fileName

	for n := 0; n < b.N; n++ {
		mainFileName, trackFileName, _ = Generate(setting)
	}

	os.Remove(mainFileName)
	os.Remove(trackFileName)
}
