package labyrinth

const RowDefault = 20
const RowMin = 20
const RowMax = 100

const ColDefault = 35
const ColMin = 20
const ColMax = 50

const FileDefault = "labyrinth.png"

const ColorModeMin = 0
const ColorModeMax = 1
const ColorModeDefault = 0

const ComplexityMin = 0
const ComplexityMax = 4
const ComplexityDefault = ComplexityMax

type Setting struct {
	File       string
	Col        int
	Row        int
	ColorMode  int
	Complexity int
}

func (s *Setting) processing() {
	if s.File == "" {
		s.File = FileDefault
	}
	if s.Col < ColMin || s.Col > ColMax {
		s.Col = ColDefault
	}
	if s.Row < RowMin || s.Row > RowMax {
		s.Row = RowDefault
	}
	if s.ColorMode < ColorModeMin || s.ColorMode > ColorModeMax {
		s.ColorMode = ColorModeDefault
	}
	if s.Complexity < ComplexityMin || s.Complexity > ComplexityMax {
		s.Complexity = ComplexityDefault
	}
}
