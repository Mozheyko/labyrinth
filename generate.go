package labyrinth

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func Generate(setting *Setting) (string, string, error) {
	setting.processing()

	// create lab
	var lab = newLab(setting)

	// create cells
	lab.createCells()

	// delete rand wall
	lab.deleteRandWall()

	// worms
	lab.createWorms()
	lab.runWorms()

	// create images file
	return lab.createImageFiles()
}
