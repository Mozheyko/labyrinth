package labyrinth

import (
	"testing"
)

func TestGetBGColor(t *testing.T) {
	if getBGColor(ColorMode_BGBlack_LineWhite) != ColorBlack {
		t.Errorf("Error GetBGColor color: %v, want: %v", getBGColor(ColorMode_BGBlack_LineWhite), ColorBlack)
	}
	if getBGColor(ColorMode_BGWhite_LineBlack) != ColorWhite {
		t.Errorf("Error GetBGColor color: %v, want: %v", getBGColor(ColorMode_BGWhite_LineBlack), ColorBlack)
	}
	if getBGColor(999) != ColorBGDefault {
		t.Errorf("Error GetBGColor color: %v, want: %v", getBGColor(999), ColorBGDefault)
	}
}

func TestGetLineColor(t *testing.T) {
	if getLineColor(ColorMode_BGBlack_LineWhite) != ColorWhite {
		t.Errorf("Error getLineColor color: %v, want: %v", getLineColor(ColorMode_BGBlack_LineWhite), ColorWhite)
	}
	if getLineColor(ColorMode_BGWhite_LineBlack) != ColorBlack {
		t.Errorf("Error getLineColor color: %v, want: %v", getLineColor(ColorMode_BGWhite_LineBlack), ColorBlack)
	}
	if getLineColor(999) != ColorLineDefault {
		t.Errorf("Error getLineColor color: %v, want: %v", getLineColor(999), ColorLineDefault)
	}
}
