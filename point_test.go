package labyrinth

import (
	"testing"
)

func TestGetTopPoints(t *testing.T) {
	x := 0
	y := 0
	color := ColorWhite

	points := getTopPoints(x, y, color)
	count := 0
	for i := x; i < x+CellSide; i++ {
		for j := y; j < y+LineSide; j++ {
			for _, p := range points {
				if p.x == i && p.y == j {
					count++
					continue
				}
			}
		}
	}

	if count != len(points) {
		t.Errorf("Error GetTopPoints count point: %d, want: %d", count, len(points))
	}
}

func TestGetBottomPoints(t *testing.T) {
	x := 0
	y := 0
	color := ColorWhite

	points := getBottomPoints(x, y, color)
	count := 0
	for i := x; i < x+CellSide; i++ {
		for j := y + CellSide - LineSide; j < y+CellSide; j++ {
			for _, p := range points {
				if p.x == i && p.y == j {
					count++
					continue
				}
			}
		}
	}

	if count != len(points) {
		t.Errorf("Error GetTopPoints count point: %d, want: %d", count, len(points))
	}
}

func TestGetLeftPoints(t *testing.T) {
	x := 0
	y := 0
	color := ColorWhite

	points := getLeftPoints(x, y, color)

	count := 0
	for i := x; i < x+LineSide; i++ {
		for j := y; j < y+CellSide; j++ {
			for _, p := range points {
				if p.x == i && p.y == j {
					count++
					continue
				}
			}
		}
	}

	if count != len(points) {
		t.Errorf("Error GetTopPoints count point: %d, want: %d", count, len(points))
	}
}

func TestGetRightPoints(t *testing.T) {
	x := 0
	y := 0
	color := ColorWhite

	points := getRightPoints(x, y, color)
	count := 0
	for i := x + CellSide - LineSide; i < x+CellSide; i++ {
		for j := y; j < y+CellSide; j++ {
			for _, p := range points {
				if p.x == i && p.y == j {
					count++
					continue
				}
			}
		}
	}

	if count != len(points) {
		t.Errorf("Error GetTopPoints count point: %d, want: %d", count, len(points))
	}
}

func TestGetCenterPoints(t *testing.T) {
	x := 0
	y := 0
	color := ColorWhite

	points := getCenterPoints(x, y, color)
	count := 0
	for i := x + CellSide/2 - 2; i < x+CellSide/2+4; i++ {
		for j := y + CellSide/2 - 2; j < y+CellSide/2+4; j++ {
			for _, p := range points {
				if p.x == i && p.y == j {
					count++
					continue
				}
			}
		}
	}

	if count != len(points) {
		t.Errorf("Error GetTopPoints count point: %d, want: %d", count, len(points))
	}
}
