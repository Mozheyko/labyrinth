package labyrinth

import "testing"

func TestCreateWorms(t *testing.T) {
	setting := NewSettingTest()
	lab := newLab(setting)
	lab.createWorms()

	if len(lab.worms) == 0 {
		t.Errorf("Error CreateWorms. Not found worms")
	}
}

func TestCreateBasicWorm(t *testing.T) {
	setting := NewSettingTest()
	lab := newLab(setting)
	lab.createBasicWorm()

	if lab.worms[0].mode != WormMain {
		t.Errorf("Error CreateBasicWorm. Not found main worm")
	}
}

func TestCreateFailWorm(t *testing.T) {
	setting := NewSettingTest()
	lab := newLab(setting)
	lab.createFailWorm()

	if lab.worms[0].mode != WormFail {
		t.Errorf("Error CreateBasicWorm. Not found fail worm")
	}
}
