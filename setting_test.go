package labyrinth

import "testing"

func TestSettingRow(t *testing.T) {
	settingDefault := NewSettingTest()

	arrIn := []int{RowMin - 1, RowMin, RowMin + 1, RowMax - 1, RowMax, RowMax + 1}
	arrOut := []int{RowDefault, RowMin, RowMin + 1, RowMax - 1, RowMax, RowDefault}

	for i, value := range arrIn {
		settingDefault.Row = value
		settingDefault.processing()
		if settingDefault.Row != arrOut[i] {
			t.Errorf("Error range row. Want %d, have %d, ", arrOut[i], settingDefault.Row)
		}
	}
}

func TestSettingCol(t *testing.T) {
	settingDefault := NewSettingTest()

	arrIn := []int{ColMin - 1, ColMin, ColMin + 1, ColMax - 1, ColMax, ColMax + 1}
	arrOut := []int{ColDefault, ColMin, ColMin + 1, ColMax - 1, ColMax, ColDefault}

	for i, value := range arrIn {
		settingDefault.Col = value
		settingDefault.processing()
		if settingDefault.Col != arrOut[i] {
			t.Errorf("Error setting - range col. Want %d, have %d", arrOut[i], settingDefault.Row)
		}
	}
}

func TestSettingFile(t *testing.T) {
	settingDefault := NewSettingTest()

	arrIn := []string{"", FileDefault}
	arrOut := []string{FileDefault, FileDefault}

	for i, value := range arrIn {
		settingDefault.File = value
		settingDefault.processing()
		if settingDefault.File != arrOut[i] {
			t.Errorf("Error setting file. Want %s, have %s", arrOut[i], settingDefault.File)
		}
	}
}

func TestSettingColorMode(t *testing.T) {
	settingDefault := NewSettingTest()

	arrIn := []int{ColorModeMin - 1, ColorModeMin, ColorModeMin + 1, ColorModeMax - 1, ColorModeMax, ColorModeMax + 1}
	arrOut := []int{ColorModeDefault, ColorModeMin, ColorModeMin + 1, ColorModeMax - 1, ColorModeMax, ColorModeDefault}

	for i, value := range arrIn {
		settingDefault.ColorMode = value
		settingDefault.processing()
		if settingDefault.ColorMode != arrOut[i] {
			t.Errorf("Error setting colorMode. Want %d, have %d", arrOut[i], settingDefault.ColorMode)
		}
	}
}

func TestSettingComplexity(t *testing.T) {
	settingDefault := NewSettingTest()

	arrIn := []int{ComplexityMin - 1, ComplexityMin, ComplexityMin + 1, ComplexityMax - 1, ComplexityMax, ComplexityMax + 1}
	arrOut := []int{ComplexityDefault, ComplexityMin, ComplexityMin + 1, ComplexityMax - 1, ComplexityMax, ComplexityDefault}

	for i, value := range arrIn {
		settingDefault.Complexity = value
		settingDefault.processing()
		if settingDefault.Complexity != arrOut[i] {
			t.Errorf("Error setting colorMode. Want %d, have %d", arrOut[i], settingDefault.Complexity)
		}
	}
}

func NewSettingTest() *Setting {
	return &Setting{
		File:       FileDefault,
		Col:        ColDefault,
		Row:        RowDefault,
		ColorMode:  ColorModeDefault,
		Complexity: ComplexityDefault,
	}
}
