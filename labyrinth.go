package labyrinth

import (
	"image"
	"image/png"
	"os"
)

const LineSide = 2  // px
const CellSide = 24 // px

type Labyrinth struct {
	img     *image.RGBA
	cells   [][]Cell
	worms   []Worm
	setting *Setting
}

func newLab(setting *Setting) *Labyrinth {
	rest := image.Rect(
		0,
		0,
		setting.Col*CellSide+(2*LineSide),
		setting.Row*CellSide+(2*LineSide),
	)
	lab := &Labyrinth{
		img:     image.NewRGBA(rest),
		cells:   [][]Cell{},
		setting: setting,
	}
	return lab
}

func (lab *Labyrinth) createImageFiles() (string, string, error) {
	mainFileName, err := lab.createMainImageFile()
	if err != nil {
		return "", "", err
	}

	trackFileName, err := lab.createTrackImageFile()
	if err != nil {
		return "", "", err
	}

	return mainFileName, trackFileName, nil
}

func (lab *Labyrinth) createMainImageFile() (string, error) {
	fileName := lab.setting.File + ".png"
	f, err := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return "", err
	}

	lab.draw()

	err = png.Encode(f, lab.img)
	if err != nil {
		return "", err
	}

	err = f.Close()
	if err != nil {
		return "", err
	}

	return fileName, nil
}

func (lab *Labyrinth) createTrackImageFile() (string, error) {
	fileName := lab.setting.File + "_track.png"
	f, err := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return "", err
	}

	lab.draw()
	lab.drawCenterCells()

	err = png.Encode(f, lab.img)
	if err != nil {
		return "", err
	}

	err = f.Close()
	if err != nil {
		return "", err
	}

	return fileName, nil
}
