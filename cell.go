package labyrinth

import (
	"image/color"
	"math/rand"
)

type Wall struct {
	top    bool
	right  bool
	bottom bool
	left   bool
}

type Cell struct {
	row    int
	col    int
	points CellPoints
	wall   Wall
}

type CellPoints struct {
	corner []Point
	center []Point
	top    []Point
	right  []Point
	bottom []Point
	left   []Point
}

func (lab *Labyrinth) createCells() {
	var x = LineSide
	var y = LineSide
	var labCells = make([][]Cell, lab.setting.Row)
	for row := 0; row < lab.setting.Row; row++ {
		labCells[row] = make([]Cell, lab.setting.Col)

		for col := 0; col < lab.setting.Col; col++ {
			var corner = getCornerPoints(x, y, getLineColor(lab.setting.ColorMode))
			var top = getTopPoints(x, y, getLineColor(lab.setting.ColorMode))
			var right = getRightPoints(x, y, getLineColor(lab.setting.ColorMode))
			var bottom = getBottomPoints(x, y, getLineColor(lab.setting.ColorMode))
			var left = getLeftPoints(x, y, getLineColor(lab.setting.ColorMode))
			var center = getCenterPoints(x, y, getBGColor(lab.setting.ColorMode))
			cell := Cell{
				row: row,
				col: col,
				points: CellPoints{
					corner: corner,
					top:    top,
					bottom: bottom,
					left:   left,
					right:  right,
					center: center,
				},
				wall: Wall{true, true, true, true},
			}

			labCells[row][col] = cell
			x += CellSide
		}

		x = LineSide
		y += CellSide
	}
	lab.cells = labCells
}

func (lab *Labyrinth) deleteRandWall() {

	for _, cells := range lab.cells {
		for _, cell := range cells {
			randSize := rand.Intn(3)

			if randSize == TOP {
				lab.deleteTopBorderCell(cell.row, cell.col)
			}
			if randSize == BOTTOM {
				lab.deleteBottomBorderCell(cell.row, cell.col)
			}
			if randSize == LEFT {
				lab.deleteLeftBorderCell(cell.row, cell.col)
			}
			if randSize == RIGHT {
				lab.deleteRightBorderCell(cell.row, cell.col)
			}
		}
	}
}

func (lab *Labyrinth) deleteTopBorderCell(row, col int) {
	if row > 0 && row <= lab.setting.Row-1 {
		lab.cells[row][col].points.top = drawPoint(lab.cells[row][col].points.top, getBGColor(lab.setting.ColorMode))
		lab.cells[row][col].wall.top = false

		lab.cells[row-1][col].points.bottom = drawPoint(lab.cells[row-1][col].points.bottom, getBGColor(lab.setting.ColorMode))
		lab.cells[row-1][col].wall.bottom = false
	}
}

func (lab *Labyrinth) deleteBottomBorderCell(row, col int) {
	if row >= 0 && row < lab.setting.Row-1 {
		lab.cells[row][col].points.bottom = drawPoint(lab.cells[row][col].points.bottom, getBGColor(lab.setting.ColorMode))
		lab.cells[row][col].wall.bottom = false

		lab.cells[row+1][col].points.top = drawPoint(lab.cells[row][col].points.bottom, getBGColor(lab.setting.ColorMode))
		lab.cells[row+1][col].wall.top = false
	}
}

func (lab *Labyrinth) deleteLeftBorderCell(row, col int) {
	if col > 0 && col <= lab.setting.Col-1 {
		lab.cells[row][col].points.left = drawPoint(lab.cells[row][col].points.left, getBGColor(lab.setting.ColorMode))
		lab.cells[row][col].wall.left = false

		lab.cells[row][col-1].points.right = drawPoint(lab.cells[row][col-1].points.right, getBGColor(lab.setting.ColorMode))
		lab.cells[row][col-1].wall.right = false
	}
}

func (lab *Labyrinth) deleteRightBorderCell(row, col int) {
	if col >= 0 && col < lab.setting.Col-1 {
		lab.cells[row][col].points.right = drawPoint(lab.cells[row][col].points.right, getBGColor(lab.setting.ColorMode))
		lab.cells[row][col].wall.right = false

		lab.cells[row][col+1].points.left = drawPoint(lab.cells[row][col+1].points.left, getBGColor(lab.setting.ColorMode))
		lab.cells[row][col+1].wall.left = false
	}
}

func (lab *Labyrinth) markCellCenter(row, col int, color color.Color) {
	lab.cells[row][col].points.center = drawPoint(lab.cells[row][col].points.center, color)
}
