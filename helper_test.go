package labyrinth

import (
	"math/rand"
	"testing"
	"time"
)

func TestRandInt(t *testing.T) {
	rand.Seed(time.Now().UTC().UnixNano())

	min := 5
	max := 10

	r := randInt(min, max)
	for i := 0; i <= 100; i++ {
		if r < min || r > max {
			t.Errorf("Error Generate range: %v", r)
		}

		if r != randInt(min, max) {
			return
		}
	}
	t.Errorf("Error randInt. No random int")
}
