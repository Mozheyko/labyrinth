package labyrinth

import (
	"log"
	"os"
	"strings"
	"testing"
)

func TestNewLab(t *testing.T) {
	setting := NewSettingTest()
	lab := newLab(setting)

	if lab.setting != setting {
		t.Errorf("Error newLab. Error setting")
	}
}

func TestCreateImageFiles(t *testing.T) {
	fileNameTest := "test"
	setting := NewSettingTest()
	setting.File = fileNameTest
	lab := newLab(setting)
	mainFileName, trackFileName, err := lab.createImageFiles()

	if err != nil {
		t.Errorf("Error Generate: %v", err)
	}
	if strings.Index(mainFileName, fileNameTest) == -1 {
		t.Errorf("Error Generate mainFileName not found")
	}
	if strings.Index(trackFileName, fileNameTest) == -1 {
		t.Errorf("Error Generate mainFileName not found")
	}

	err = os.Remove(mainFileName)
	if err != nil {
		log.Println(err)
	}
	err = os.Remove(trackFileName)
	if err != nil {
		log.Println(err)
	}
}

func TestCreateMainImageFile(t *testing.T) {
	fileNameTest := "test"
	setting := NewSettingTest()
	setting.File = fileNameTest
	lab := newLab(setting)
	fileName, err := lab.createMainImageFile()

	if err != nil {
		t.Errorf("Error Generate: %v", err)
	}
	if strings.Index(fileName, fileNameTest) == -1 {
		t.Errorf("Error Generate mainFileName not found")
	}

	err = os.Remove(fileName)
	if err != nil {
		log.Println(err)
	}
}

func TestCreateTrackImageFile(t *testing.T) {
	fileNameTest := "test"
	setting := NewSettingTest()
	setting.File = fileNameTest
	lab := newLab(setting)
	fileName, err := lab.createTrackImageFile()

	if err != nil {
		t.Errorf("Error Generate: %v", err)
	}
	if strings.Index(fileName, fileNameTest) == -1 {
		t.Errorf("Error Generate mainFileName not found")
	}

	err = os.Remove(fileName)
	if err != nil {
		log.Println(err)
	}
}
